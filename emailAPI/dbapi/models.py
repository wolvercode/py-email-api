#!/usr/bin/python
import httplib, json
# import config
from emailAPI import app

class Model:
    def __init__(self):
        self.config = app.config
        
    def apiRequest(self, method, reqURL, params, headers):
        apiConn = httplib.HTTPConnection(self.config['dbapi']['host']+":"+self.config['dbapi']['port'])
        if params and headers :
            apiConn.request(method, "/"+self.config['dbapi']['url']+"/"+reqURL, params, headers)
        else :
            apiConn.request(method, "/"+self.config['dbapi']['url']+"/"+reqURL)

        return apiConn.getresponse()

    def getSMTPConfig(self):
        print("[ Getting SMTP Configuration ] :"),
        smtpConfig = {}
        outResponse = self.apiRequest("GET", "configuration/getSMTPConfig", "", "")
        print(outResponse.reason)
        if outResponse.reason == 'OK':
            responseData = outResponse.read()
            smtpConfig = json.loads(responseData)

        return smtpConfig
