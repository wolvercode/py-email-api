from flask import Flask, jsonify
import config

app = Flask(__name__)
app.config.update(config.config)

from emailAPI.dbapi.views import dbAPI
app.register_blueprint(dbAPI)

from emailAPI.views import emailapis
app.register_blueprint(emailapis)
