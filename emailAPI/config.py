#!/usr/bin/python
import os,json

class Config:
	def __init__(self):
		with open(os.path.dirname(__file__)+'/config.json', 'r') as f:
			configValue = json.load(f)
			self.currentConfig = configValue[configValue['active']]

config = Config().currentConfig