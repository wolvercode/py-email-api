#!/usr/bin/alert

from collections import defaultdict

template = defaultdict(dict)

template['first']['attachment']=["tsl.png"]
template['first']['html']="""\
<html>
    <div style="background-color:#fafafa;font:16px Arial,Helvetica,sans-serif;padding:5px 10px;">
        <div style="margin:15px 0px 0px 50px"><img src="cid:tsl.png" width="120px"></div>
            <div style="background-color:#fff;border:thin solid #eceff1;border-radius:5px;border-radius:5px;margin: 50px;margin-top:10px;">
                <div style="margin:0;border-top-right-radius:5px;border-top-left-radius:5px;background: #cb2d3e;background: -webkit-linear-gradient(to right, #ef473a, #cb2d3e);background:linear-gradient(to right, #ef473a, #cb2d3e);font-size:20px;color:#fff;padding:8px 15px;">
                    <p style="float:left;">$emailProgramTitle$</p>
                    <p style="float:right;">[ $emailSubject$ ]</p>
                    <div style="clear: both;"></div>
                </div>
                <div style="margin:10px;padding: 5px;border-radius:5px;background:#fff;">
                    $emailMessage$
                </div>
                <div style="margin:0;border-top:thin solid #eceff1;border-bottom-right-radius:5px;border-bottom-left-radius:5px;padding:15px;text-align:center;color:#474a52;">Swamedia @2018 - Hubungi team support kami untuk info lebih lengkap</div>
    </div>
</html>
"""

template['second']['attachment']=["tsl.png","bell.png"]
template['second']['html']="""\
<html>
  <div style="background-color:#fafafa;font:16px Arial,Helvetica,sans-serif;padding:15px 10px;">
    <div style="margin:0 auto; margin-top:15px; max-width:680px;">
        <img src="cid:tsl.png" width="120px">
    </div>
    <div style="background-color:#fff;border:thin solid #eceff1;border-radius:5px;border-radius:5px;margin:0 auto; margin-top:10px; margin-bottom:50px; max-width:680px;">
        <div style="margin:0;border-top-right-radius:5px;border-top-left-radius:5px;background: #cb2d3e;background: -webkit-linear-gradient(to right, #ef473a, #cb2d3e);background:linear-gradient(to right, #ef473a, #cb2d3e);font-size:20px;color:#fff;padding:8px 15px;">
            <p style="float:left; margin-top:5px; margin-bottom:10px; font-size:14px; letter-spacing:1px;">$emailProgramTitle$</p>
            <div style="clear: both;"></div>
            <div style="text-align:center;">
                <img src="cid:bell.png" width="80" />
            </div>
            <p style="margin-top:10px; margin-bottom:10px; text-align:center; font-weight:bold; letter-spacing:2px;">$emailSubject$</p>
        </div>
        <div style="margin:10px;padding: 5px;border-radius:5px;background:#fff;">
            $emailMessage$
        </div>
        <div style="margin:0;border-top:thin solid #eceff1;border-bottom-right-radius:5px;border-bottom-left-radius:5px;padding:15px;text-align:center;color:#474a52; font-size:15px;">
            @ Swamedia 2019 - Hubungi team support kami untuk info lebih lengkap
    </div>
    </div>
</html>
"""

template['third']['attachment']=["tsl.png","bell.png","support.png"]
template['third']['html']="""\
<html>
  <div style="background-color:#fafafa;font:16px Arial,Helvetica,sans-serif;padding:15px 10px;">
    <div style="margin:0 auto; margin-top:15px; max-width:700px;">
        <img src="cid:tsl.png" width="120px">
    </div>
    <div style="background-color:#fff;border:thin solid #eceff1;border-radius:5px;border-radius:5px;margin:0 auto; margin-top:10px; margin-bottom:10px; max-width:700px;">
        <div style="margin:0;border-top-right-radius:5px;border-top-left-radius:5px;background: #cb2d3e;background: -webkit-linear-gradient(to right, #ef473a, #cb2d3e);background:linear-gradient(to right, #ef473a, #cb2d3e);font-size:20px;color:#fff;padding:8px 15px;">
            <p style="float:right; margin-top:5px; margin-bottom:10px; font-size:14px; letter-spacing:1px;">$emailProgramTitle$</p>
            <div style="clear: both;"></div>
            <div style="text-align:center;">
                <img src="cid:bell.png" width="80" />
            </div>
            <p style="margin-top:10px; margin-bottom:10px; text-align:center; font-weight:bold; letter-spacing:2px;">$emailSubject$</p>
        </div>
        <div style="margin:10px;padding: 5px;border-radius:5px;background:#fff;">
            $emailMessage$
        </div>
        <div style="height:4px; background-color:#f37e75; margin-left:15px; margin-right:15px; border-radius:2px;"></div>
        <div style="margin:0;border-bottom-right-radius:5px;border-bottom-left-radius:5px;padding:10px;text-align:center;color:#474a52; font-size:14px;">
            <p style="margin:0;">
                <img src="cid:support.png" width="25px" style="vertical-align:middle; margin-right:8px;" />
                <span>Hubungi team support kami untuk info lebih lanjut</span>
            </p>
        </div>
    </div>
    <div style="text-align:center; font-size:12px; margin-bottom:20px; color:#999999;">
      <p>Mohon untuk tidak mengirim balasan, email ini dikirimkan secara otomatis oleh sistem.</p>
      <p style="margin:0;">PT Swamedia Informatika | 2019</p>
    </div>
</html>
"""