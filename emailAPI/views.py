#!/usr/bin/python
import smtplib, os,sys, glob, gzip, shutil
from smtplib import *
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText 
from email.mime.base import MIMEBase 
from email import encoders
from flask import Blueprint, request, jsonify
from emailAPI import app
from template import template

emailapis = Blueprint('emailapis', __name__)

@emailapis.route('/sendMail', methods=['POST'])
def sendEmailMessage():
    '''
     --- Send Email Parameters in JSON format ---
        {
            "email_receiver" : "example@example.com", 
            "email_title" : "Email Title Example", 
            "email_subject" : "Email Subject Example",
            "email_message" : "Email Message Example",
            "email_attachment" : "['/home/example/a.jpg','/home/example/b.jpg'] #this is optional"
        }
    '''

    parameters = request.get_json(silent=True)
    if not parameters:
        return jsonify({"Status": {"Error":"Request Body must not empty"}})

    if not {"email_receiver","email_title","email_subject","email_message"} <= set(parameters):
        errorMsg = {
            "Status": "Error",
            "Message": "Some required Parameters is not defined in your json body",
            "ExampleRequiredParameter" : {
                "email_receiver" : "example@example.com", 
                "email_title" : "Email Title Example", 
                "email_subject" : "Email Subject Example",
                "email_message" : "Email Message Example",
                "email_attachment" : "['/home/example/a.jpg','/home/example/b.jpg'] #this is optional"
            }
        }

        return jsonify(errorMsg)
    
    smtpConfig = app.config['smtpConfig']
    
    emailTemplate = template[app.config['main']['emailTemplate']]['html']
    emailTemplateAttachment = template[app.config['main']['emailTemplate']]['attachment']

    msg = MIMEMultipart()
    alertMessage = emailTemplate.replace('$emailSubject$', parameters['email_subject'])
    alertMessage = alertMessage.replace('$emailMessage$', parameters['email_message'])
    alertMessage = alertMessage.replace('$emailProgramTitle$', app.config['main']['emailprogramtitle'])
    msg.attach(MIMEText(alertMessage,'html'))
    msg['Subject'] = "[ "+app.config['main']['emailprogramtitle']+" ] "+parameters['email_title']
    msg['To'] = ', '.join(parameters['email_receiver'])
    
    assetsNum = 0
    for usedAssets in emailTemplateAttachment:
        if glob.glob(sys.path[0]+"/assets/"+usedAssets):
            usedAssetsFile = os.path.basename(sys.path[0]+"/assets/"+usedAssets)
            usedAssetsOpen = open(sys.path[0]+"/assets/"+usedAssets, "rb") 
            usedAssetsAttachment = MIMEBase('image', 'png') 
            usedAssetsAttachment.set_payload((usedAssetsOpen).read()) 
            encoders.encode_base64(usedAssetsAttachment) 
            usedAssetsAttachment.add_header('Content-Disposition', "attachment; filename= %s" % usedAssetsFile) 
            usedAssetsAttachment.add_header('X-Attachment-Id', str(assetsNum))
            usedAssetsAttachment.add_header('Content-ID', '<'+usedAssets+'>')
            msg.attach(usedAssetsAttachment) 
            assetsNum += 1
    
    if parameters.has_key('email_attachment') :
        for emailAttachment in parameters['email_attachment']:
            if not emailAttachment or not glob.glob(emailAttachment):
                continue

            if os.stat(emailAttachment).st_size >= 10000000 :
                fname = emailAttachment
                emailAttachment = emailAttachment+".gz"
                with open(fname, 'rb') as f_in, gzip.open(emailAttachment, 'wb') as f_out:
                    shutil.copyfileobj(f_in, f_out)

            if os.stat(emailAttachment).st_size <= 25000000 :
                filename = os.path.basename(emailAttachment)
                attachment = open(emailAttachment, "rb") 
                msgAttachment = MIMEBase('application', 'octet-stream') 
                msgAttachment.set_payload((attachment).read()) 
                encoders.encode_base64(msgAttachment) 
                msgAttachment.add_header('Content-Disposition', "attachment; filename= %s" % filename) 
                msg.attach(msgAttachment)

    messageTxt = msg.as_string()
    
    try:
        server = smtplib.SMTP_SSL(smtpConfig['host'], int(smtpConfig['port']))
    except:
        return jsonify({"Status":"Failed","Message":"Could not connect to server"})
    
    try:
        server.login(smtpConfig['user'],smtpConfig['pass'])
    except SMTPAuthenticationError as e:
        server.quit()
        return jsonify({"Status":"Failed","Message":"Could not login to smtp server"})
    
    try:
        server.sendmail(smtpConfig['user'], parameters['email_receiver'], messageTxt)
        return jsonify({"Status":"Success","Message":"Send mail successful"})
    except:
        return jsonify({"Status":"Failed","Message":"Could not send mail"})
    finally:
        server.quit()
    
    return jsonify({"Status":"Success","Message":"Send mail successful"})
